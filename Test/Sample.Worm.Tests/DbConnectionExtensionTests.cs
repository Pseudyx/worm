﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Worm.Kernel.Data;
using Worm.Kernel.Tests.Domain;

namespace Worm.Kernel.Tests
{
    public class DbConnectionExtensionTests
    {
        private DbConnection _connection;
        private readonly Guid _orgId = Guid.Parse("1c0812d6-2a53-4896-838e-92b008171efe");

        [OneTimeSetUp]
        public void SetupFixture()
        {
            var connectionSettings = ConfigurationManager.ConnectionStrings["Worm"];
            var connectionString = connectionSettings.ConnectionString;
            var connectionFactory = DbProviderFactories.GetFactory(connectionSettings.ProviderName);
            _connection = connectionFactory.CreateConnection();
            _connection.ConnectionString = connectionString;

        }

        [Test]
        public void UpsertOrganisation()
        {
            // Arrange
            Exception exc = null;

            var org = new Organisation
            {
                Id = _orgId,
                Name = "New Business"
            };

            
            //Act
            _connection.Open();
            try
            {
                _connection.ExecuteFromResource<DbConnectionExtensionTests>("Sql.UpsertOrganisation.sql", new
                {
                    OrganisationId = org.Id,
                    Name = org.Name
                });
            }
            catch (Exception ex)
            {
                exc = ex;
            }
            finally
            {
                _connection.Close();
            }
            
            //Assert
            Assert.IsNull(exc);
        }


        [Test]
        public void GetOrganisation()
        {
            //Arrange 
            Exception exc = null;
            Organisation result = null;

            //Act
            _connection.Open();
            try { 
            result = _connection.QuerySingleFromResource<Organisation, DbConnectionExtensionTests>("Sql.Organisation.sql", new
            {
                OrganisationId = _orgId
            });
            }
            catch (Exception ex)
            {
                exc = ex;
            }
            finally
            {
                _connection.Close();
            }

            //Assert
            Assert.NotNull(result);
            Assert.IsNull(exc);
        }

    }
}

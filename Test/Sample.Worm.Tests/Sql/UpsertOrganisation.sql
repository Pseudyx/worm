﻿DECLARE @exist bit
if exists (Select * from [Organisation] where Id = @OrganisationId)
	set @exist = 1
else 
	set @exist = 0

IF @exist = 1
	BEGIN
	--Update
	UPDATE [Organisation]
		SET Name = @Name
		WHERE [Id] = @OrganisationId
	
	END
ELSE
	BEGIN
	--Insert
	INSERT INTO [Organisation]
           (Id
           ,Name)
     VALUES
           (@OrganisationId
           ,@Name)
	END
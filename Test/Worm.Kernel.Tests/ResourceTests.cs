﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Worm.Kernel.Utility;

namespace Worm.Kernel.Tests
{
    public class ResourceTests
    {
        [Test]
        public void Get_ExistingResource_ReturnsContents()
        {
            // Arrange
            const string resourceName = "Resources.ExampleResource.txt";
            
            // Act
            var contents = Resource.Get<ResourceTests>(resourceName);
            
            // Assert
            Assert.AreEqual("HelloWorld", contents);
        }

        [Test]
        public void Get_NonExistingResource_ThrowsException()
        {
            // Arrange
            const string resourceName = "Resources._no_resource_.txt";

            // Act && Assert
            Assert.Throws<ResourceNotFoundException>(() => Resource.Get<ResourceTests>(resourceName));
        }

        [Test]
        public void Get_NullName_ThrowsException()
        {
            // Act && Assert
            Assert.Throws<ArgumentNullException>(() => Resource.Get<ResourceTests>(null));
        }
    }
}

﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Worm.Kernel.Utility;

namespace Worm.Kernel.Data
{
    public static class DbConnectionExtension
    {
        public static void ExecuteFromResource<TAssembly>(this IDbConnection connection, string resourceName,
            object sqlParameters = null)
        {
            var sqlCmd = Resource.Get<TAssembly>(resourceName);
            connection.Execute(sqlCmd, sqlParameters);
        }

        public static IEnumerable<T> QueryFromResource<T, TAssembly>(this IDbConnection connection, string resourceName,
            object sqlParameters = null)
        {
            var sqlCmd = Resource.Get<TAssembly>(resourceName);
            return connection.Query<T>(sqlCmd, sqlParameters);
        }

        public static T QuerySingleFromResource<T, TAssembly>(this IDbConnection connection, string resourceName,
            object sqlParameters = null)
        {
            var sqlCmd = Resource.Get<TAssembly>(resourceName);
            return connection.Query<T>(sqlCmd, sqlParameters).FirstOrDefault();
        }
    }
}